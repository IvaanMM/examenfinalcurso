<?php

 use yii\data\ActiveDataProvider;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <p>Los ciclistas cuya edad está entre 20 y 40 años mostrando únicamente el dorsal y el nombre del ciclista</p>

$query = Post::find()->where(['state_id' => 1]);

    $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10,
        ],
        'sort' => [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
                'title' => SORT_ASC,
            ]
        ],
    ]);
    $posts = $provider->getModels();
    <p>Las etapas circulares mostrando sólo el numero de etapa y longitud de las mismas</p>
    
        $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10,
        ],
        'sort' => [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
                'title' => SORT_ASC,
            ]
        ],
    ]);
    
    <p>Los puertos con altura mayor a 1000 metro figurando el nombre del puerto y el dorsal del ciclista que lo gano</p>
        
        $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10,
        ],
        'sort' => [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
                'title' => SORT_ASC,
            ]
        ],
    ]);
</div>
